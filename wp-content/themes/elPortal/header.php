<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?><?php if (get_bloginfo('description', 'display')) { echo " - ".get_bloginfo('description', 'display'); } ?></title>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width"
<meta name="description" content="El Portal a la Consciencia. La puerta de entrada hacia tu mundo interior. Vilcabamba, Ecuador" />
<meta name="keywords" content="Hatha Yoga, Meditación, Tacto Esencial, Masaje, Relajación, Vilcabamba" />
<meta name="Language" content="es" />
<meta name="Author" content="Sokvist" />
<link href="//www.google-analytics.com" rel="dns-prefetch">
<link rel="canonical" href="http://www.elportalconsciente.org/">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/touch-icon-ipad-retina.png">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory') ?>/ie_hacks.css" />
<![endif]-->

<!-- Open Graph 
    <meta property="og:locale" content="es_ES">
    <meta property="og:type" content="website">
    <meta property="og:title" content="El Portal a la Consciencia">
    <meta property="og:description" content="El Portal a la Consciencia. La puerta de entrada hacia tu mundo interior. Vilcabamba, Ecuador">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/og-image.jpg">
    <meta property="og:url" content="http://www.elportalconsciente.org/">
    <meta property="og:site_name" content="El Portal a la Consciencia"> -->

<?php wp_head(); ?>

<!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>assets/js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vendor/modernizr.custom.js"></script>
<![endif]-->

</head>

<body <?php body_class(); ?>>

<div id="wrap">

	<div id="header">

		<!-- language -->
		<div class="language">
			<?php qtrans_generateLanguageSelectCode('text') ?>
		</div>
		<!-- /language -->

		<div id="bg-logo">
			<div id="logo">
				<h1><a href="<?php echo home_url(); ?>/" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					<?php if (qtrans_getLanguage() == 'es') : ?>
					el portal<br><span>a la consciencia</span>
					<?php else : ?>
					the portal<br><span class="eng">to consciousness</span>
					<?php endif; ?>
				</a></h1>

				<div class="description">
					<?php if (qtrans_getLanguage() == 'es') : ?>
					la puerta de entrada<br> hacia tu mundo interior
					<?php else : ?>
					the gateway<br> to your inner world
					<?php endif; ?>
				</div>
			</div>
		</div>

		<div id="menu-wrapper">
			<div id="menu">
				<?php wp_nav_menu(); ?>
			</div>

			<div id="sub-menu">
				<?php wp_nav_menu( array('menu' => 'menu-2' )); ?>
			</div>
		</div>

	</div><!-- #header -->

	<div id="social">

	<div id="subscribe">

		<h2>
			<?php if (qtrans_getLanguage() == 'es') : ?>
			Suscr&iacute;bete
			<?php else : ?>
			Subscribe
			<?php endif; ?>
		</h2>

			 <ul> 
			 <!--<li class="email"><a href="#mailform" class="toggle id" title="Suscr&iacute;bete por e-mail">Suscripci&oacute;n por e-mail</a></li>-->
			 <li class="rss"><a href="<?php bloginfo('rss2_url'); ?>" title="Suscr&iacute;bete via RSS">Suscripci&oacute;n via RSS</a></li>
			 </ul>

	</div><!-- #subscribe -->

	<div id="visit">

		<h2>
			<?php if (qtrans_getLanguage() == 'es') : ?>
			Vis&iacute;tanos
			<?php else : ?>
			Visit Us
			<?php endif; ?>
		</h2>

			<ul>
				<li class="facebook"><a href="https://www.facebook.com/pages/El-Portal-a-la-Consciencia/731356883607159" title="Vis&iacute;tanos en Facebook">Facebook</a></li>
				<!--<li class="flickr"><a href="#" title="Vis&iacute;tanos en Flickr" class="tooltip">Flickr</a></li>-->
			</ul>

	</div><!-- #visit -->

	

	<!--<div id="mailform">

        	<div class="inner">

        	<p>Rellena los datos para suscribirte a nuestro newsletter</p>

		<?php if (class_exists('MailPress')) MailPress::form(); ?>

                </div>

	</div>	 #mailform -->

	</div><!-- #social -->