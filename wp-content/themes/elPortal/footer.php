<?php

/**
 * @package WordPress
 * @subpackage loupYoga_Theme
 */

?>

	<div id="bg-footer">

		<div id="footer">

			<div id="footer-top">

				<ul id="menu-footer">
					<?php wp_nav_menu( array('menu' => 'menu-footer' )); ?>
				</ul>

			</div><!-- #footer-top -->

			<div id="search">

				<h3>Buscar palabra</h3>

					<?php get_search_form(); ?>

			</div><!-- #search -->

			<div id="footer-btm">

		        <ul id="menu-footer-admin"> 
					<li><a href="<?php bloginfo('home') ?>/wp-admin/" title="P&aacute;gina para la administraci&oacute;n de la web">admin</a></li>
		            <li><a href="<?php bloginfo('home') ?>/mapa-web" title="Ver el mapa del sitio web">mapa web</a></li>
		            <li><a href="http://www.jorgenmortensen.eu/" title="J&oslash;rgen Mortensen, dise&ntilde;ador web">dise&ntilde;o web: &Oslash;</a></li>
					<!--<li><a href="http://validator.w3.org/check?uri=referer" title="XHTML 1.0 v&aacute;lido!">XHTML 1.0</a></li>-->
			  		<!--<li><a href="http://jigsaw.w3.org/css-validator/check/referer?profile=css3" title="CSS v&aacute;lido!">CSS</a></li>-->
					<li><a href="<?php echo home_url(); ?>/" title="El Portal de Yoga">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></a></li>
				</ul>

	        </div><!-- #footer-btm -->

		</div><!-- #footer -->
		
	</div><!-- #bg-footer -->

	<div class="cboth"></div>

</div><!-- #wrap -->


<!-- <?php echo get_num_queries(); ?> queries. <?php timer_stop(1); ?> seconds. -->

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/easy.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

<?php wp_footer(); ?>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-3566548-20', 'auto');
  ga('send', 'pageview');

</script>

</body>

</html>