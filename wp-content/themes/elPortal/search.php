<?php
/**
 * @package WordPress
 * @subpackage loupYoga_Theme
 */

get_header(); ?>

	<div id="col-left">

	<?php if (have_posts()) : ?>

		<h3>Resultados de la b&uacute;squeda</h3>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?>>
				<h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<p><small><?php the_time('l, F jS, Y') ?></small></p>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>

		<?php endwhile; ?>

		<div class="navigation">
			<div class="fleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="fright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
		</div>

	<?php else : ?>

		<h3>Resultados de la b&uacute;squeda...</h3>
		<p>Lo sentimos, ninguna entrada no coincide con tus criterios de b&uacute;squeda.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>

	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
