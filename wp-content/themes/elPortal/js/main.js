$(document).ready(function(){	
	$.easy.navigation();
	$.easy.tooltip();
	$.easy.popup();
	$.easy.external();
	$.easy.rotate({
	selector: '.rotate',
	pause: 7000,
	randomize:false
	});
	$.easy.forms();
	$.easy.showhide();
	$.easy.jump();
});