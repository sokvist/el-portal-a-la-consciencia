<?php

/**

 * @package WordPress

 * @subpackage loupYoga_Theme

 */



get_header(); ?>



	<?php

		$options = get_option('greenleaf_theme_options');

		if ( ($options['greenleaf_main_punchline'] != "") || ($options['greenleaf_headline'] != "")) {

	?>

		<div id="banner">

			<?php

				if ($options['greenleaf_main_punchline'] != "") echo "<div id=\"banner-headline\">".$options['greenleaf_main_punchline']."</div>";

				if ($options['greenleaf_headline'] != "") echo "<div id=\"banner-secondary\">".$options['greenleaf_headline']."</div>";

			?>

		</div>

	<?php } ?>

	<br />

	

	<div id="elcentro">

		<div id="elcentro-left">

        	<?php if (qtrans_getLanguage() == 'es') : ?>
			<h2>Habitando el CUERPO ..... <span class="green">Bienestar</span></h2>
        	<h2>Silenciando la MENTE ..... <span class="green">Calma</span></h2>
        	<h2>Alimentando el ESPÍRITU ..... <span class="green">Equilibrio</span></h2>
			<?php else : ?>
			<h2>Inhabiting the BODY ..... <span class="green">Wellness</span></h2>
        	<h2>Silencing the MIND ..... <span class="green">Calm</span></h2>
        	<h2>Fedding the SPIRIT ..... <span class="green">Balance</span></h2>
			<?php endif; ?>

        </div><!-- #elcentro-left -->

        <div id="elcentro-right">

			<div class="rotate">
				<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-01.jpg" width="520" height="346" alt="El Portal" />
				<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-02.jpg" width="520" height="346" alt="El Portal" />
				<!--<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-03.jpg" width="560" height="373" alt="El Portal" />-->
				<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-04.jpg" width="520" height="346" alt="El Portal" />
				<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-05.jpg" width="520" height="346" alt="El Portal" />
				<img src="<?php bloginfo('template_directory'); ?>/images/home-slider-06.jpg" width="520" height="346" alt="El Portal" />
			</div>

		</div><!-- #elcentro-right -->

	</div><!-- #elcentro -->

	

	<div id="actualidad"><a href ="http://www.elportaldeyoga.org/?cat=1,3,5,6,23,24,30" title="Ver todas las actividades">
		<?php if (qtrans_getLanguage() == 'es') : ?>
		Actualidad
		<?php else : ?>
		News
		<?php endif; ?>
	</a></div>


	<div class="col3-wrapper">
	<?php if (have_posts()) : ?>

	

		<?php $counter = 0; ?>

		<?php while (have_posts()) : the_post(); ?>

			<div class="col3">

				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">

					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>



					<div class="entry">

						<?php the_excerpt('Read the rest of this entry &raquo;'); ?>

						<div class="morepage-list"><?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?></div>

						<p><br /><a href="<?php the_permalink() ?>" class="button">Leer m&aacute;s &rarr;</a></p>

					</div>



				</div>

			</div>

			<?php

				$counter++;

				if ($counter % 3 == 0) echo '<div class="cboth"></div>' ;

			?>

		<?php endwhile; ?>



		<div class="cboth pagination">

			<?php greenleaf_pagenavi(); ?>

		</div>



	<?php else : ?>



		<h1 class="acenter">Not Found</h1>

		<p class="acenter">Sorry, but you are looking for something that isn't here.</p>

		<?php get_search_form(); ?>



	<?php endif; ?>

	</div>


<?php get_footer(); ?>

