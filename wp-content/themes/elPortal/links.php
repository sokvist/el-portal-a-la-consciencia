<?php
/**
 * @package WordPress
 * @subpackage loupYoga_Theme
 */

/*
Template Name: Links
*/
?>

<?php get_header(); ?>

	<div id="col-left">

		<h2>Links:</h2>
		<ul>
		<?php wp_list_bookmarks(); ?>
		</ul>

	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
