<?php
/**
 * @package WordPress
 * @subpackage loupYoga_Theme
 */

get_header(); ?>

	<div id="col-left">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<h2><?php the_title(); ?></h2>
			
			<div id="bg-entry-top"></div>
			<div id="bg-entry-body">
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<div id="morepage-list"><?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?></div>
				
				<div class="cboth"></div>
			</div>
			</div><!-- #bg-entry-body -->
			<div id="bg-entry-btm"></div>
		</div>
		
		<?php comments_template(); ?>
		
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
