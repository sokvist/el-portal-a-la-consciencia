<?php /* Template Name: Front Page */ get_header(); ?>


	<main role="main">
		
		<!-- home-text -->
		<section class="home-text">
			<?php query_posts(array( 'post_type' => 'home-text' )); ?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<article class="post">
						
				<div class="the-content">
					<?php the_content(); ?>
				</div>
				<!-- /the-content -->
						
			</article>

			<?php endwhile; ?>
			<?php else : ?>
				
			<article class="post error">
				<h1 class="404">Nothing has been posted like that yet</h1>
			</article>

			<?php endif; ?>

		</section>
		<!-- /home-text -->

		<!-- featured-posts -->
		<section class="featured-posts contain clear">
			<?php if (have_posts()) : ?>
			<?php query_posts(array( 'post_type' => 'news' , 'posts_per_page' => 4 )); ?>
			<?php while (have_posts()) : the_post(); ?>

			<article class="post one-fourth grid">
						
				<div class="the-content pad">
					<?php
					if ( has_post_thumbnail()):
					    the_post_thumbnail('homepage-thumb', array('class' => 'aligncenter'));
					endif;
					?>

					<h2 class="title title-home">
						<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</h2>
					<?php html5wp_excerpt('html5wp_index'); ?>

				</div>
				<!-- /the-content -->
						
			</article>

			<?php endwhile; ?>
				
			<!-- pagintation -->
			<!--<div id="pagination" class="clearfix">
				<div class="past-page"><?php previous_posts_link( 'newer' ); ?></div>
				<div class="next-page"><?php next_posts_link( 'older' ); ?></div>
			</div>-->
			<!-- /pagination -->

			<?php else : ?>
				
			<article class="post error">
				<h2 class="404">Nothing has been posted like that yet</h2>
			</article>

			<?php endif; ?>

		</section>
		<!-- /featured-posts -->

		<!-- testimonials -->
		<section class="testimonials contain2 clear">

			<div class="ttmn-left three-fourth grid">

				<h2>Testimonios</h2>
				<div class="banner">
					<div class="unslider-arrow hide-text">
				    	<div class="next">Next</div>
				    </div>
				    <ul>
				    	<?php query_posts(array( 'post_type' => 'testimonials', 'posts_per_page' => 13 )); ?>
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				        <li>
				        	<h3><?php the_title(); ?></h3>
				        	<?php
								global $more;
								$more = 0;
							?>

							<?php the_content('Leer más &raquo;'); ?>
				        </li>
				        <?php endwhile; ?>
				    </ul>
			    </div>
			    
				<?php else : ?>
					
				<div class="banner post error">
					<h2 class="404">Nothing has been posted like that yet</h2>
				</div>

				<?php endif; ?>

		    </div>
		    <div class="ttmn-right one-fourth grid0">
		    	<h3><a href="<?php echo home_url(); ?>/calendario/" title="Calendario">Calendario &raquo;</a></h3>
		    </div>
		</section>
		<!-- /testimonials -->

	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
