<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" lang="en"> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" lang="en"> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html <?php language_attributes(); ?> class="no-js">
<!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width">
		<meta name="description" content="" />
    	<meta name="keywords" content="" />
    	<meta name="author" content="Sokvist" />

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link rel="canonical" href="http://www.elportal.com/">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/apple-touch-icon.png" />
    	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/touch-icon-ipad-retina.png">
    	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">

		<!-- Open Graph -->
	    <meta property="og:locale" content="ca_ES">
	    <meta property="og:type" content="website">
	    <meta property="og:title" content="El Portal">
	    <meta property="og:description" content="">
	    <meta property="og:image" content="http://www.elportal.com/assets/images/sokvist-web-og-image.jpg">
	    <meta property="og:url" content="http://www.elportal.com/">
	    <meta property="og:site_name" content="El Portal">

		<?php wp_head(); ?>
		
		<!--[if lt IE 9]>
	        <script src="<?php echo get_template_directory_uri(); ?>assets/js/vendor/html5-3.6-respond-1.1.0.min.js"></script>
	    <![endif]-->

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

				<!-- language -->
				<div class="language">
					<?php qtrans_generateLanguageSelectCode('text') ?>
				</div>
				<!-- /language -->

					<!-- logo -->
					<div class="logo">
						<a href="<?php echo home_url(); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/images/el_portal-logo.png" alt="El Portal" class="logo-img" width="571" height="238">
						</a>
					</div>
					<!-- /logo -->

					<h1 class="hide-text"><?php bloginfo('name'); ?>: <?php bloginfo('description'); ?></h1>

					<!-- nav -->
					<nav class="nav" role="navigation">
						<?php html5blank_nav(); ?>
					</nav>
					<!-- /nav -->

			</header>
			<!-- /header -->
