/* =============================================================================
   jQuery: all the custom stuff!
   ========================================================================== */

/**
*** Slide menu http://tympanus.net/codrops/2013/04/17/slide-and-push-menus/
**/
(function() {

	var menuRight = document.getElementById( 'cbp-spmenu-s2' ),
		showRight = document.getElementById( 'showRight' ),
		body = document.body;

		showRight.onclick = function() {
			classie.toggle( this, 'active' );
			classie.toggle( menuRight, 'cbp-spmenu-open' );
			classie.toggle( body, 'navToLeft' ); /* Move page nav to left */
			disableOther( 'showRight' );
		};

		function disableOther( button ) {
			if( button !== 'showRight' ) {
				classie.toggle( showRight, 'disabled' );
			}
		}

}());

/**
 * FullPage plugin https://github.com/alvarotrigo/fullPage.js
 */
(function() {

        $.fn.fullpage({
            //scrollingSpeed: 900,
            navigation: true,
			navigationPosition: 'right',
			navigationTooltips: ['Fem', 'Som', 'Com'],
			anchors: ['fem', 'som', 'com'],
            easing: 'easeInQuart',
            resize: false,
            verticalCentered: false,
            afterRender: function(){
                $('#section1-fader').delay(1000).fadeOut(1500);
            },
            afterLoad: function(anchorLink, index){

                    //section 2
                    if(index == '2'){
                        $('#overlay-left').delay(100).fadeTo( 1000, 1 );
                        $('#overlay-right').delay(700).fadeTo( 1000, 1 );
                    }
                    else {
                        $('#overlay-left').fadeOut();
                        $('#overlay-right').fadeOut();
                    }

                    //section 3
                    if(index == '3'){
                        $('#modal').delay(300).fadeTo( 500, 1 );
                    }
                    else {
                        $('#modal').fadeOut();
                    }
                },
        });

}());

 /**
 * http://tympanus.net/codrops/2014/02/06/fullscreen-overlay-effects/
 */
(function() {
	var triggerBttn = document.getElementById( 'trigger-overlay' ),
		overlay = document.querySelector( 'div.overlay' ),
		closeBttn = overlay.querySelector( 'button.overlay-close' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( classie.has( overlay, 'open' ) ) {
			classie.remove( overlay, 'open' );
			classie.add( overlay, 'close' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay, 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !classie.has( overlay, 'close' ) ) {
			classie.add( overlay, 'open' );
		}
	}

	triggerBttn.addEventListener( 'click', toggleOverlay );
	closeBttn.addEventListener( 'click', toggleOverlay );
})();

(function() {
	var triggerBttn2 = document.getElementById( 'trigger-overlay2' ),
		overlay2 = document.querySelector( 'div.overlay2' ),
		closeBttn2 = overlay2.querySelector( 'button.overlay-close2' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( classie.has( overlay2, 'open2' ) ) {
			classie.remove( overlay2, 'open2' );
			classie.add( overlay2, 'close2' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay2, 'close2' );
			};
			if( support.transitions ) {
				overlay2.addEventListener( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !classie.has( overlay2, 'close2' ) ) {
			classie.add( overlay2, 'open2' );
		}
	}

	triggerBttn2.addEventListener( 'click', toggleOverlay );
	closeBttn2.addEventListener( 'click', toggleOverlay );
})();

